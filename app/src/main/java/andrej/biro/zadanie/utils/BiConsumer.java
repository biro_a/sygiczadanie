package andrej.biro.zadanie.utils;

public interface BiConsumer<T, U> {
    void accept(T var1, U var2);
}
