package andrej.biro.zadanie.utils;

import android.content.Context;
import android.support.annotation.StringRes;
import android.support.v7.app.AlertDialog;

import andrej.biro.zadanie.utils.Action1;

public class AlertDialogFactory {

    private AlertDialogFactory() {}

    public static AlertDialog create(Context context, @StringRes int title, @StringRes int positiveButtonTitle,
                                     @StringRes int negativeButtonTitle, Action1 positiveAction){
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
        alertDialogBuilder.setPositiveButton(positiveButtonTitle, (dialogInterface, i1) -> positiveAction.call());
        alertDialogBuilder.setNegativeButton(negativeButtonTitle, (dialogInterface, i12) -> dialogInterface.cancel());
        alertDialogBuilder.setTitle(title);
        return alertDialogBuilder.create();
    }


}
