package andrej.biro.zadanie.model;

import android.os.Parcel;
import android.os.Parcelable;

public class SettingsData implements Parcelable{
    private int polylineSize;
    private int polylineColor;
    private int interval;

    public SettingsData(int polylineSize, int polylineColor, int interval) {
        this.polylineSize = polylineSize;
        this.polylineColor = polylineColor;
        this.interval = interval;
    }

    private SettingsData(Parcel in) {
        this.polylineSize = in.readInt();
        this.polylineColor = in.readInt();
        this.interval = in.readInt();
    }

    public int getPolylineSize() {
        return polylineSize;
    }

    public int getPolylineColor() {
        return polylineColor;
    }

    public int getInterval() {
        return interval;
    }

    public void copy(SettingsData newSettingsData) {
        this.polylineSize = newSettingsData.polylineSize;
        this.polylineColor = newSettingsData.polylineColor;
        this.interval = newSettingsData.interval;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.polylineSize);
        dest.writeInt(this.polylineColor);
        dest.writeInt(this.interval);
    }

    public static final Creator<SettingsData> CREATOR = new Creator<SettingsData>() {
        @Override
        public SettingsData createFromParcel(Parcel source) {
            return new SettingsData(source);
        }

        @Override
        public SettingsData[] newArray(int size) {
            return new SettingsData[size];
        }
    };

    @Override
    public boolean equals(Object obj) {
        SettingsData other = (SettingsData) obj;
        if (other.getPolylineSize() != this.polylineSize) return false;
        if (other.getPolylineColor() != this.polylineColor) return false;
        return other.getInterval() == this.interval;
    }
}

