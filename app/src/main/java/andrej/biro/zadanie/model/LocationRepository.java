package andrej.biro.zadanie.model;


import android.location.Location;
import android.support.annotation.Nullable;

import com.google.android.gms.maps.model.LatLng;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class LocationRepository {
    private static final float MIN_DISTANCE_IN_METERS = 100.0f;
    private static LocationRepository instance;
    private final List<LocationData> locations;
    private final EdgePoints edgePoints;

    public static LocationRepository getInstance() {
        if (instance == null) {
            instance = new LocationRepository();
        }

        return instance;
    }

    private LocationRepository() {
        locations = new ArrayList<>();
        edgePoints = new EdgePoints();
        locations.add(new LocationData(48.129643, 17.112576, 5.0f, new Date(), "fused"));
        locations.add(new LocationData(48.115584, 17.155758, 5.0f, new Date(), "fused"));
        locations.add(new LocationData(48.103147, 17.117260, 5.0f, new Date(), "fused"));
        locations.add(new LocationData(48.112667, 17.085318, 5.0f, new Date(), "fused"));
    }

    public void addLocation(Location location) {
        LocationData locationData = new LocationData(location);
        locations.add(locationData);
        checkEdgePoints(new LatLng(location.getLatitude(), location.getLongitude()));
    }

    // if new location is closer than 100 meters to any other location, then it will be not added to list
    public boolean canAddLocationToList(Location location) {
        for (LocationData locationData : locations) {
            float[] distance = new float[1];
            Location.distanceBetween(locationData.getLatitude(), locationData.getLongitude(),
                    location.getLatitude(), location.getLongitude(), distance);

            if (distance[0] < MIN_DISTANCE_IN_METERS) {
                return false;
            }
        }

        return true;
    }

    public List<LocationData> getLocationData() {
        return locations;
    }

    public EdgePoints getEdgePoints() {
        return edgePoints;
    }

    public void checkEdgePoints(LatLng point) {
        if (edgePoints.getWestPoint() == null || edgePoints.getWestPoint().longitude > point.longitude) {
            edgePoints.setWestPoint(point);
        }

        if (edgePoints.getEastPoint() == null || edgePoints.getEastPoint().longitude < point.longitude) {
            edgePoints.setEastPoint(point);
        }

        if (edgePoints.getNorthPoint() == null || edgePoints.getNorthPoint().latitude < point.latitude) {
            edgePoints.setNorthPoint(point);
        }

        if (edgePoints.getSouthPoint() == null || edgePoints.getSouthPoint().latitude > point.latitude) {
            edgePoints.setSouthPoint(point);
        }
    }

    @Nullable public LocationData get(Integer key) {
        return locations.get(key);
    }
}
