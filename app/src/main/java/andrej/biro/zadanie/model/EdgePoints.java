package andrej.biro.zadanie.model;

import android.location.Location;

import com.google.android.gms.maps.model.LatLng;

public class EdgePoints {
    private LatLng southPoint;
    private LatLng westPoint;
    private LatLng eastPoint;
    private LatLng northPoint;

    public LatLng getSouthPoint() {
        return southPoint;
    }

    public void setSouthPoint(LatLng southPoint) {
        this.southPoint = southPoint;
    }

    public LatLng getWestPoint() {
        return westPoint;
    }

    public void setWestPoint(LatLng westPoint) {
        this.westPoint = westPoint;
    }

    public LatLng getEastPoint() {
        return eastPoint;
    }

    public void setEastPoint(LatLng eastPoint) {
        this.eastPoint = eastPoint;
    }

    public LatLng getNorthPoint() {
        return northPoint;
    }

    public void setNorthPoint(LatLng northPoint) {
        this.northPoint = northPoint;
    }
}
