package andrej.biro.zadanie.ui;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import andrej.biro.zadanie.R;
import andrej.biro.zadanie.model.SettingsData;

public class NavigationActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    private static final String SHARED_PREFERENCE_NAME = "SYGIC_ZADANIE";
    private static final String SETTINGS_POLYLINE_SIZE_KEY = "SETTINGS_POLYLINE_SIZE";
    private static final String SETTINGS_POLYLINE_COLOR_KEY = "SETTINGS_POLYLINE_COLOR";
    private static final String SETTINGS_LOCATION_UPDATED_INTERVAL_KEY = "SETTINGS_LOCATION_UPDATED_INTERVAL";
    private static final int DEFAULT_POLYLINE_SIZE = 2;
    private static final int DEFAULT_POLYLINE_COLOR = Color.RED;
    private static final int DEFAULT_LOCATION_UPDATE_INTERVAL = 10;
    private static final int SETTINGS_ACTIVITY_REQUEST_CODE = 1;

    private SettingsData settingsData;
    private Fragment visibleFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(andrej.biro.zadanie.R.layout.activity_navigation);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        if (settingsData == null) {
            settingsData = loadSettingsDataFromSharedPreferences();
        }

        if (savedInstanceState == null ) {
            FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
            visibleFragment = MapFragment.getInstance(settingsData);
            fragmentTransaction.replace(R.id.container, visibleFragment);
            fragmentTransaction.commit();
            navigationView.setCheckedItem(R.id.nav_map);
            getSupportActionBar().setTitle(R.string.title_map);
        }

    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == Activity.RESULT_OK && requestCode == SETTINGS_ACTIVITY_REQUEST_CODE) {
            SettingsData settingsData = data.getParcelableExtra(SettingsActivity.SETTINGS_DATA_EXTRA_KEY);
            saveSettingsDataToSharedPreferences(settingsData);
        }
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle settings view item clicks here.
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        int id = item.getItemId();
        int titleId = R.string.title_map;

        if (id == R.id.nav_map) {
            visibleFragment = MapFragment.getInstance(settingsData);
        } else if (id == R.id.nav_about) {
            titleId = R.string.title_about;
            visibleFragment = new AboutFragment();
        } else if (id == R.id.nav_settings) {
            Intent settingsIntent = SettingsActivity.createIntent(getApplicationContext(), settingsData);
            startActivityForResult(settingsIntent, SETTINGS_ACTIVITY_REQUEST_CODE);
        }

        if (visibleFragment != null) {
            fragmentTransaction.replace(R.id.container, visibleFragment);
            fragmentTransaction.commit();
        }

        getSupportActionBar().setTitle(titleId);
        invalidateOptionsMenu();

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private SettingsData loadSettingsDataFromSharedPreferences() {
        SharedPreferences sharedpreferences = getSharedPreferences(SHARED_PREFERENCE_NAME, Context.MODE_PRIVATE);
        int polylineSize = sharedpreferences.getInt(SETTINGS_POLYLINE_SIZE_KEY, DEFAULT_POLYLINE_SIZE);
        int polylineColor = sharedpreferences.getInt(SETTINGS_POLYLINE_COLOR_KEY, DEFAULT_POLYLINE_COLOR);
        int locationUpdateInterval = sharedpreferences.getInt(SETTINGS_LOCATION_UPDATED_INTERVAL_KEY, DEFAULT_LOCATION_UPDATE_INTERVAL);

        return new SettingsData(polylineSize, polylineColor, locationUpdateInterval);
    }

    private void saveSettingsDataToSharedPreferences(SettingsData settingsData) {
        this.settingsData.copy(settingsData);

        SharedPreferences sharedpreferences = getSharedPreferences(SHARED_PREFERENCE_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedpreferences.edit();
        editor.putInt(SETTINGS_POLYLINE_SIZE_KEY, settingsData.getPolylineSize());
        editor.putInt(SETTINGS_POLYLINE_COLOR_KEY, settingsData.getPolylineColor());
        editor.putInt(SETTINGS_LOCATION_UPDATED_INTERVAL_KEY, settingsData.getInterval());

        editor.apply();
    }
}
