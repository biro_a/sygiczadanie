package andrej.biro.zadanie.ui;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.text.DateFormat;

import andrej.biro.zadanie.model.LocationData;
import andrej.biro.zadanie.R;
import andrej.biro.zadanie.databinding.DialogFragmentLocationDetailBinding;

public class LocationDetailDialogFragment extends DialogFragment {
    private static final String LOCATION_DATA_EXTRA_KEY = "LOCATION_DATA";

    private LocationData locationData;

    public static LocationDetailDialogFragment getInstance(LocationData locationData) {
        LocationDetailDialogFragment fragment = new LocationDetailDialogFragment();
        Bundle bundle = new Bundle();
        bundle.putParcelable(LOCATION_DATA_EXTRA_KEY, locationData);
        fragment.setArguments(bundle);

        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle args = getArguments();
        if (args != null) {
            locationData = args.getParcelable(LOCATION_DATA_EXTRA_KEY);
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        DialogFragmentLocationDetailBinding binding = DataBindingUtil.inflate(
                inflater, R.layout.dialog_fragment_location_detail, container, false);
        binding.setLocation(locationData);

        String location = locationData.getLatitude() + ", " + locationData.getLongitude();
        String formattedDate = DateFormat.getDateTimeInstance().format(locationData.getDate());

        binding.locationValue.setText(location);
        binding.dateValue.setText(formattedDate);

        return binding.getRoot();
    }
}
