package andrej.biro.zadanie.ui;


import android.Manifest;
import android.arch.lifecycle.LifecycleFragment;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.LocationManager;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import andrej.biro.zadanie.R;
import andrej.biro.zadanie.model.LocationData;
import andrej.biro.zadanie.model.LocationLiveData;
import andrej.biro.zadanie.model.LocationRepository;
import andrej.biro.zadanie.model.SettingsData;
import andrej.biro.zadanie.utils.AlertDialogFactory;
import andrej.biro.zadanie.utils.BiConsumer;
import io.victoralbertos.rx2_permissions_result.RxPermissionsResult;


public class MapFragment extends LifecycleFragment implements OnMapReadyCallback, GoogleMap.OnMarkerClickListener{
    private static final String TAG = MapFragment.class.getName();
    private static final String SETTINGS_DATA_EXTRA_KEY = "SETTINGS_DATA";

    private static final String LOCATION_DIALOG_TAG = "LocationDialog";
    private static final String[] PERMISSIONS_ACCESS_LOCATION = {
            Manifest.permission.ACCESS_FINE_LOCATION
    };

    private GoogleMap map;
    private final LocationRepository locationRepository;
    private SettingsData settingsData;

    public static MapFragment getInstance(SettingsData settingsData) {
        MapFragment fragment = new MapFragment();
        Bundle args = new Bundle();
        args.putParcelable(SETTINGS_DATA_EXTRA_KEY, settingsData);
        fragment.setArguments(args);

        return fragment;
    }

    public MapFragment() {
        locationRepository = LocationRepository.getInstance();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle args = getArguments();
        if (args != null) {
            settingsData = args.getParcelable(SETTINGS_DATA_EXTRA_KEY);
        }
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        checkPermissions();
        checkGpsEnabled();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_map, container, false);
        SupportMapFragment mapFragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        view.findViewById(R.id.arrow_up).setOnClickListener(view1 -> moveToPoint(locationRepository.getEdgePoints().getNorthPoint()));
        view.findViewById(R.id.arrow_right).setOnClickListener(view12 -> moveToPoint(locationRepository.getEdgePoints().getEastPoint()));
        view.findViewById(R.id.arrow_down).setOnClickListener(view13 -> moveToPoint(locationRepository.getEdgePoints().getSouthPoint()));
        view.findViewById(R.id.arrow_left).setOnClickListener(view14 -> moveToPoint(locationRepository.getEdgePoints().getWestPoint()));
        return view;
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        map = googleMap;
        map.setOnMarkerClickListener(this);

        List<LocationData> locationData = locationRepository.getLocationData();
        for (LocationData testPoint : locationData) {
            locationRepository.checkEdgePoints(testPoint.getPoint());
        }

        if (!locationData.isEmpty()) {
            createPathBetweenAllPoints(locationData);
            LocationData lastPoint = locationData.get(locationData.size() - 1);
            moveToPoint(lastPoint.getPoint());
        }
    }

    @Override
    public boolean onMarkerClick(Marker marker) {
        Integer snippetValue = Integer.valueOf(marker.getSnippet());
        LocationData locationData = locationRepository.get(snippetValue);

        FragmentTransaction ft = getChildFragmentManager().beginTransaction();
        Fragment prev = getFragmentManager().findFragmentByTag(LOCATION_DIALOG_TAG);
        if (prev != null) {
            ft.remove(prev);
        }
        ft.addToBackStack(null);

        // Create and show the dialog.
        LocationDetailDialogFragment newFragment = LocationDetailDialogFragment.getInstance(locationData);
        newFragment.show(ft, LOCATION_DIALOG_TAG);

        return false;
    }

    private void checkPermissions() {
        RxPermissionsResult.on(this).requestPermissions(PERMISSIONS_ACCESS_LOCATION)
                .subscribe(result ->
                        result.targetUI()
                                .handlePermissionStatus(result.permissions(), result.grantResults())
                );

    }

    private void handlePermissionStatus(String[] permissions, int[] grantResults) {
        for (int i = 0; i < permissions.length; i++) {
            boolean granted = grantResults[i] == PackageManager.PERMISSION_GRANTED;
            if (granted) {
                startObservingLocation();
            } else {
                AlertDialog alertDialog = AlertDialogFactory.create(getContext(), R.string.location_deny_dialog, R.string.enable,
                        R.string.cancel, this::checkPermissions);
                alertDialog.show();
            }
        }
    }

    private void checkGpsEnabled() {
        LocationManager service = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);
        boolean enabled = service
                .isProviderEnabled(LocationManager.GPS_PROVIDER);
        if ( !enabled) {
            AlertDialog alertDialog = AlertDialogFactory.create(getContext(), R.string.gps_module_is_disabled_dialog_message, R.string.yes,
                    R.string.no, () -> startActivity(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS)));
            alertDialog.show();
        }
    }

    private void startObservingLocation() {
        LocationRequest locationRequest = createLocationRequest();
        LocationLiveData.getInstance(getActivity(), locationRequest).observe(this, location -> {
            if (map != null) {
                if (locationRepository.canAddLocationToList(location)) {
                    LatLng position = new LatLng(location.getLatitude(), location.getLongitude());
                    List<LocationData> locationData = locationRepository.getLocationData();
                    String snippet = String.valueOf(locationData.size());
                    map.addMarker(new MarkerOptions().position(position).snippet(snippet));
                    locationRepository.addLocation(location);
                    newPointAddedToCollection(locationData);
                }
            } else {
                Log.e(TAG, "Map is not ready");
            }
        });
    }

    private void newPointAddedToCollection(List<LocationData> locations) {
        List<LocationData> locationData = new ArrayList<>(locations);
        if (locationData.size() > 1) {
            LatLng srcPoint = locationData.get(locationData.size() - 2).getPoint();
            LatLng dstPoint = locationData.get(locationData.size() - 1).getPoint();
            createPathBetweenTwoPoints(srcPoint, dstPoint);

        } else {
            LatLng firstPoint = locationData.get(0).getPoint();
            moveToPoint(firstPoint);
        }
    }

    private void createPathBetweenAllPoints(Collection<LocationData> locationData) {
        tupleIterator(locationData, (src, dst) -> {
            createPathBetweenTwoPoints(src.getPoint(), dst.getPoint());
        });

        int i = 0;
        for (LocationData location : locationData) {
            String snippet = String.valueOf(i++);
            map.addMarker(new MarkerOptions().position(location.getPoint()).snippet(snippet));
        }

    }

    private void createPathBetweenTwoPoints(LatLng srcPoint, LatLng destPoint) {
        PolylineOptions polylineOptions = new PolylineOptions();
        polylineOptions.add(srcPoint, destPoint);
        polylineOptions.width(settingsData.getPolylineSize());
        polylineOptions.color(settingsData.getPolylineColor());
        map.addPolyline(polylineOptions);
    }

    private <T> void tupleIterator(Iterable<T> iterable, BiConsumer<T, T> consumer) {
        Iterator<T> it = iterable.iterator();
        if(!it.hasNext()) return;
        T first = it.next();

        while(it.hasNext()) {
            T next = it.next();
            consumer.accept(first, next);
            first = next;
        }
    }

    private void moveToPoint(@Nullable LatLng point) {
        if (point != null) {
            map.moveCamera(CameraUpdateFactory.newLatLngZoom(point, 15));
        }
    }

    private LocationRequest createLocationRequest(){
        int interval = this.settingsData.getInterval();

        return LocationRequest.create()
                .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
                .setInterval(interval * 1000)
                .setFastestInterval((interval * 1000) / 2);
    }
}
