package andrej.biro.zadanie.ui;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Handler;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.SeekBar;
import android.widget.TextView;

import com.pavelsikun.vintagechroma.ChromaDialog;
import com.pavelsikun.vintagechroma.IndicatorMode;
import com.pavelsikun.vintagechroma.colormode.ColorMode;

import andrej.biro.zadanie.R;
import andrej.biro.zadanie.model.SettingsData;

public class SettingsActivity extends AppCompatActivity {
    public static final String SETTINGS_DATA_EXTRA_KEY = "SETTINGS_DATA";
    private SeekBar polylineSeekBar;
    private EditText locationIntervalEditText;
    private CoordinatorLayout settingsCoordinatorLayout;
    private View polylineColorView;
    private TextView polylineSizeValue;
    private int polylineColor;
    private boolean clickOnBack = false;
    private SettingsData settingsData;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

        Bundle args = getIntent().getExtras();
        if (args != null) {
            settingsData = args.getParcelable(SETTINGS_DATA_EXTRA_KEY);
        }

        settingsCoordinatorLayout = findViewById(R.id.settings_coordinator_layout);
        polylineSeekBar = findViewById(R.id.polyline_seekBar);
        locationIntervalEditText = findViewById(R.id.location_update_interval);
        polylineColorView = findViewById(R.id.color_view);
        polylineSizeValue = findViewById(R.id.polyline_size_value);
        polylineColorView.setOnClickListener(view1 -> createColorPickerDialog());
        polylineSeekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                polylineSizeValue.setText(String.valueOf(seekBar.getProgress()));
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        if (settingsData != null) {
            polylineColor = settingsData.getPolylineColor();
            polylineSeekBar.setProgress(settingsData.getPolylineSize());
            polylineSizeValue.setText(String.valueOf(settingsData.getPolylineSize()));
            polylineColorView.setBackgroundColor(settingsData.getPolylineColor());
            locationIntervalEditText.setText(String.valueOf(settingsData.getInterval()));
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.settings, menu);
        menu.findItem(R.id.action_settings).setVisible(false);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_done) {
            successfulFinishActivity();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        SettingsData newSettingsData = getSettingsData();
        if (clickOnBack || this.settingsData.equals(newSettingsData)) {
            super.onBackPressed();
        } else {
            clickOnBack = true;
            Snackbar snackbar = Snackbar
                    .make(settingsCoordinatorLayout, R.string.unsaved_changes, Snackbar.LENGTH_INDEFINITE)
                    .setAction(R.string.save, view -> successfulFinishActivity());

            snackbar.show();

            new Handler().postDelayed(() -> clickOnBack = false, 2000);
        }
    }

    private void createColorPickerDialog() {
        int initColor = settingsData == null ? Color.RED : settingsData.getPolylineColor();
        new ChromaDialog.Builder()
                .initialColor(initColor)
                .colorMode(ColorMode.ARGB)
                .indicatorMode(IndicatorMode.HEX)
                .onColorSelected(color -> {
                    polylineColorView.setBackgroundColor(color);
                    this.polylineColor = color;
                })
                .create()
                .show(getSupportFragmentManager(), "ChromaDialog");
    }

    private SettingsData getSettingsData() {
        int polylineSize = polylineSeekBar.getProgress();
        int locationInterval = Integer.valueOf(locationIntervalEditText.getText().toString());
        return new SettingsData(polylineSize, polylineColor, locationInterval);
    }

    private void successfulFinishActivity() {
        Intent intent = new Intent();
        intent.putExtra(SETTINGS_DATA_EXTRA_KEY, getSettingsData());
        setResult(Activity.RESULT_OK, intent);
        finish();
    }

    public static Intent createIntent(Context context, SettingsData data) {
       Intent intent = new Intent(context, SettingsActivity.class);
       intent.putExtra(SETTINGS_DATA_EXTRA_KEY, data);
       return intent;
    }

}
