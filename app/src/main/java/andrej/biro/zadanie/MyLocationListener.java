package andrej.biro.zadanie;

import android.arch.lifecycle.Lifecycle;
import android.arch.lifecycle.LifecycleObserver;
import android.arch.lifecycle.OnLifecycleEvent;
import android.content.Context;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;

public class MyLocationListener implements LifecycleObserver, LocationListener {
    private final Context context;
    private final Lifecycle lifecycle;
    private final LocationManager locationManager;
    private final String provider;
    private long minTime;
    private boolean enabled = false;

    public MyLocationListener(Context context, Lifecycle lifecycle, LocationManager locationManager) {
        this.context = context;
        this.lifecycle = lifecycle;
        this.locationManager = locationManager;
        this.provider = "";
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_START)
    void start() {
        if (enabled) {
            // connect
        }
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_RESUME)
    void resume() {
        locationManager.requestLocationUpdates(provider, minTime, 1, this);
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_STOP)
    void stop() {
        // disconnect if connected
    }

    public void enable() {
        enabled = true;

        if (lifecycle.getCurrentState().isAtLeast(Lifecycle.State.STARTED)) {
            // connect if not connected
        }
    }

    @Override
    public void onLocationChanged(Location location) {

    }

    @Override
    public void onStatusChanged(String s, int i, Bundle bundle) {

    }

    @Override
    public void onProviderEnabled(String s) {

    }

    @Override
    public void onProviderDisabled(String s) {

    }
}
