package andrej.biro.zadanie;

import android.app.Application;

import io.victoralbertos.rx2_permissions_result.RxPermissionsResult;


public class ZadanieApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();

        RxPermissionsResult.register(this);
    }
}
